# Pandoc Alpine

Tiny Alpine base container including Pandoc. The image swinkelhofer/pandoc can be used as base image in Dockerfile's ```FROM```-directive.

## Example in Dockerfile

```
FROM swinkelhofer/pandoc

RUN pandoc -h
```
