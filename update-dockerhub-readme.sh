#!/bin/ash

check_bin() {
    if ! type $1 > /dev/null 2>&1
    then
        echo "Binary $1 not found"
        exit 1
    fi
}

check_var() {
    eval "VAR=\$$1"
    if [ -z "$VAR" ]
    then
        echo "Please provide environment variable $1"
        exit 1
    fi
}

for i in "DOCKERHUB_USER" "DOCKERHUB_PASS" "DOCKERHUB_README_FILE" "DOCKERHUB_IMAGE_NAME"; do check_var $i; done

for i in "curl" "jq" "sed" "cat"; do check_bin $i; done

export TOKEN=$(curl -s -H "Content-Type: application/json" -X POST -d '{"username": "'${DOCKERHUB_USER}'", "password": "'${DOCKERHUB_PASS}'"}' https://hub.docker.com/v2/users/login  | jq -r .token)

export PAYLOAD='{"registry":"registry-1.docker.io","full_description":"'$(cat ${DOCKERHUB_README_FILE} | sed -E ':a;N;$!ba;s/\r{0,1}\n/\\n/g')'"}'

curl -X PATCH -H "Content-type: application/json" -H "Authorization: JWT ${TOKEN}" https://cloud.docker.com/v2/repositories/${DOCKERHUB_IMAGE_NAME}/ --data "${PAYLOAD}"
