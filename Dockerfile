FROM alpine:3.10 as builder

ENV PANDOC_VERSION 2.9.1.1

ENV BUILD_DEPS \
    alpine-sdk \
    coreutils \
    ghc \
    gmp \
    libffi \
    musl-dev \
    wget \
    cabal \
    linux-headers \
    zlib-dev

RUN apk add --no-cache $BUILD_DEPS && \
    wget https://hackage.haskell.org/package/pandoc-${PANDOC_VERSION}/pandoc-${PANDOC_VERSION}.tar.gz && \
    tar xf pandoc-${PANDOC_VERSION}.tar.gz && \
    cd pandoc-${PANDOC_VERSION} && \
    cabal update && \
    cabal install pandoc


FROM alpine:3.10

ENV RUNTIME_DEPS \
    gmp \
    libffi \
    linux-headers

RUN apk add ${RUNTIME_DEPS}
COPY --from=builder /root/.cabal/bin/pandoc /usr/bin/pandoc
COPY --from=builder /root/.cabal/share/x86_64-linux-ghc-8.4.3/pandoc-2.9.1.1/data/abbreviations /root/.cabal/share/x86_64-linux-ghc-8.4.3/pandoc-2.6/data/abbreviations

CMD pandoc -h
